
# coding: utf-8

# In[25]:


# AQUI FAÇO OS IMPORTES NECESSARIOS
import numpy as np
import cv2
from matplotlib import pyplot as plt

MIN_MATCH_COUNT = 10

# CARREGANDO AS IMAGENS
img1 = cv2.imread('C:\Github\python\cafe1.jpg', 0)
img2 = cv2.imread('C:\Github\python\cafe2.jpg', 0)
img3 = cv2.imread('C:\Github\python\cafe3.jpg', 0)

# CRIA AS BORDAS NA IMAGEN PARA DEPOIS PODER SOMAR ELAS
img1 = constant= cv2.copyMakeBorder(img1,400,400,400,400,cv2.BORDER_CONSTANT)
img2 = constant= cv2.copyMakeBorder(img2,400,400,400,400,cv2.BORDER_CONSTANT)
img3 = constant= cv2.copyMakeBorder(img3,400,400,400,400,cv2.BORDER_CONSTANT)

# INICIALIZACAO DO METODO SIFT
sift = cv2.xfeatures2d.SIFT_create()
# BUSCA OS KEYPOINT E OS DESCRITORES
kp1, des1 = sift.detectAndCompute(img1, None)
kp2, des2 = sift.detectAndCompute(img2, None)


# CRIANDO OS PARAMETROS PARA INICIALIZAR O FLANN
FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
search_params = dict(checks=50)

# INICIALIZACAO DO FLANN
flann = cv2.FlannBasedMatcher(index_params, search_params)

# ENCONTRANDO OS PONTOS MATCHES ENTRE A IMAGEM 1 E 2
matches = flann.knnMatch(des1, des2, k=2)

# ARMAZENA APENAS OS MATCHES BONS 
good = []
for m, n in matches:
    if m.distance < 0.7 * n.distance:
        good.append(m)
# VALIDA SE O NUMERO DE MATCHES É BOM O SUFICIENTE
if len(good) > MIN_MATCH_COUNT:
    src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    
    # GERA A MATRIZ HOMOGRAFICA PARA FAZER A CORREÇÃO DE PERSPECTIVA
    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)
    matchesMask = mask.ravel().tolist()
    
    h, w = img1.shape
    
    # FAZ A CORRECAO DA PERSPECTIVA
    imgt = cv2.warpPerspective(img1, M, (w, h))
    # GERA O MOSAICO FINAL
    imgA = cv2.addWeighted(imgt, 0.5, img2, 0.5, 1)
    
    # --------------------- PASSANDO PARA A TERCEIRA JUNÇÃO ------------------------------------
    # DECTETA OS KP E DESCRIMINADORES DA TERCEIRA IMAGEM E DA JUNÇÃO
    kp3, des3 = sift.detectAndCompute(img3, None)
    kpA, desA = sift.detectAndCompute(imgA, None)
    
    # INICIALIZACAO DO FLANN - ENCONTRANDO OS PONTOS MATCHES ENTRE A IMAGEM 3 E AUX
    flann = cv2.FlannBasedMatcher(index_params, search_params)
    matches = flann.knnMatch(des3, desA, k=2)
    
    # SEPARA APENAS OS BONS MATCHES
    goodA = []
    for m, n in matches:
        if m.distance < 0.7 * n.distance:
            goodA.append(m)
            
    if len(goodA) > MIN_MATCH_COUNT:
        src_ptsA = np.float32([kp3[m.queryIdx].pt for m in goodA]).reshape(-1, 1, 2)
        dst_ptsA = np.float32([kpA[m.trainIdx].pt for m in goodA]).reshape(-1, 1, 2)

        # GERA A MATRIZ HOMOGRAFICA PARA FAZER A CORREÇÃO DE PERSPECTIVA
        M, mask = cv2.findHomography(src_ptsA, dst_ptsA, cv2.RANSAC, 5.0)
        matchesMask = mask.ravel().tolist()

        h, w = imgA.shape
        
        # FAZ A CORRECAO DA PERSPECTIVA
        imgt = cv2.warpPerspective(img3, M, (w, h))
        # GERA O MOSAICO FINAL
        mosaic = cv2.addWeighted(imgA, 0.5, imgt, 0.8, 1)        
    
else:
    print("Not enough matches are found - %d/%d" % (len(good), MIN_MATCH_COUNT))
    matchesMask = None
    
# IMPRIME A IMAGEM
plt.imshow(imgA, 'gray'), plt.show()
plt.imshow(mosaic, 'gray'), plt.show()

